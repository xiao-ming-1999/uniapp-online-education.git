import api from "./request"

export default {
	// 获取首页数据
	getIndexData() {
		return api.get('/mobile/index')
	},
	// 获取拼团列表
	getGroupList(params) {
		params.usable =1
		return api.get('/mobile/group', params)
	},
	// 获取秒杀列表
	getFlashsaleList(params) {
		params.usable =1
		return api.get('/mobile/flashsale', params)
	},
	// 获取优惠券列表
	getCouponList() {
		return api.get('/mobile/coupon')
	},
	// 注册
	reg(data) {
		return api.post('/mobile/reg', data)
	},
	// 登录
	login(data) {
		return api.post('/mobile/login', data)
	},
	// 获取验证码
	getCaptchat(data) {
		return api.post('/mobile/get_captcha',data)
	},
	// 绑定手机号
	getBindPhone(data) {
		return api.post('/mobile/bind_mobile',data)
	},
	// 找回密码
	getForgetPwd(data) {
		return api.post('/mobile/forget',data)
	},
	// 退出登录
	loginOut(data) {
		return api.post('/mobile/logout',data)
	},
	// 修改密码
	updatePassword(data) {
		return api.post('/mobile/update_password',data)
	},
	// 上传图片
	upload(filePath,onProgress = null){
		return api.upload('/mobile/upload',{
			filePath
		},{
			onProgress
		})
	},
	// 修改资料
	updateInfo(data) {
		return api.post('/mobile/update_info',data)
	},
	// 订单列表页 
	getOrderList(params) {
		return api.get('/mobile/order/list',params)
	},
	// 领取优惠券 
	getUserCoupont(data) {
		return api.post('/mobile/user_coupon/receive',data)
	},
	// 优惠券列表
	getCoupontList(params) {
		return api.get('/mobile/user_coupon',params)
	},
	// 搜索接口
	getSearchList(params) {
		return api.get('/mobile/search',params)
	},
	// 查看课程详情
	readCourse(params) {
		return api.get('/mobile/course/read',params)
	},
	// 获取专栏列表
	readColumn(params) {
		return api.get('/mobile/column/read',params)
	},
	// 获取学习进度列表
	userHistoryList(params) {
		return api.get('/mobile/user_history/list',params)
	},
	// 更新学习进度
	updateUserHistory(data) {
		return api.post('/mobile/user_history/update',data)
	},
	// 考场列表
	getTestList(params) {
		return api.get('/mobile/testpaper/list',params)
	},
	// 开始考试
	startTest(params) {
		return api.get('/mobile/testpaper/read',params)
	},
	// 考试交卷
	userTestSava(data) {
		return api.post('/mobile/user_test/save',data)
	},
	// 考试记录
	userTestHistory(params) {
		return api.get('/mobile/user_test/list',params)
	},
	// 课程列表
	getCourseList(params) {
		return api.get('/mobile/course/list',params)
	},
	// 专栏列表
	getColumnList(params) {
		return api.get('/mobile/column/list',params)
	},
	// 购买免费课程
	learn(data) {
		return api.post('/mobile/order/learn',data)
	},
	// 获取商品数据
	getGoodsList(params) {
		return api.get('/mobile/goods/read',params)
	},
	// 获取可用优惠券列表
	getUsableCoupon(params) {
		return api.get('/mobile/user_coupon/count',params)
	},
	// 创建订单
	createOrder(data,type='save') {
		return api.post('/mobile/order/'+type,data)
	},
	// h5支付
	wxpay(data) {
		return api.post('/mobile/order/wxpay',data)
	},
	// 获取直播列表
	getLiveList(params) {
		return api.get('/mobile/live/list',params)
	},
	// 获取直播详情
	getLiveDetail(params) {
		return api.get('/mobile/live/read',params)
	},
	// 发送弹幕
	sendLiveComment(data) {
		return api.post('/mobile/live_comment/save',data)
	},
	// 获取直播弹幕
	getLiveComment(params) {
		return api.get('/mobile/live_comment',params)
	},
	// 当前拼团专栏/课程的可组团列表
	getGroupWorkList(params) {
		return api.get('/mobile/group_work/list',params)
	},
	// 微信登录
	wxLogin(data) {
		return api.post('/mobile/weixin_login',data)
	},
	// 我的电子书列表
	getMyBookList(params){
		return api.get("/mobile/mybook",params)
	},
	// 社区列表
	getBbsList(params){
		return api.get('/mobile/bbs',params)
	},
	// 帖子列表
	getPostList(params){
		return api.get('/mobile/post/list',params)
	},
	// 点赞帖子
	supportPost(data){
		return api.post('/mobile/post/support',data)
	},
	// 取消点赞帖子
	unsupportPost(data){
		return api.post('/mobile/post/unsupport',data)
	},
	// 发布帖子
	addPost(data){
		return api.post('/mobile/post/save',data)
	},
	// 查看帖子
	readPost(params){
		return api.get('/mobile/post/read',params)
	},
	// 查看评论列表
	getPostComments(params){
		return api.get('/mobile/post_comment',params)
	},
	// 发送评论
	replyPost(data){
		return api.post('/mobile/post/reply',data)
	},
	// 我的帖子列表
	getMyPost(params){
		return api.get('/mobile/mypost',params)
	},
	// 删除帖子
	deletePost(data){
		return api.post('/mobile/post/delete',data)
	},
	

}

