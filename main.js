// #ifndef VUE3
import Vue from 'vue'
import App from './App'
// 将api挂载在全局
import api from "@/api/api.js"
import store from '@/store/index.js'
// 原型挂载处
Vue.prototype.$api = api
Vue.prototype.$toast = function(title, icon = "none") {
	uni.showToast({
		title,
		icon
	})
}
Vue.prototype.$store = store

Vue.prototype.$navigateTo = function(url) {
	uni.navigateTo({
		url
	})
}

Vue.prototype.$authJump = function(url) {
	if (!store.state.userInfo) {
		this.$navigateTo('/pages/login/login')
		return
	}
	if (!store.state.userInfo.phone) {
		this.$navigateTo('/pages-user/bind-phone/bind-phone')
		return
	}
	this.$navigateTo(url)
}
// 打开网页
Vue.prototype.$openWebview = function(url){
	uni.navigateTo({
		url: '/pages/webview/webview?url='+encodeURIComponent(url),
	})
}


Vue.config.productionTip = false

App.mpType = 'app'

const app = new Vue({
	store,
	...App
})
app.$mount()
// #endif

// #ifdef VUE3
import {
	createSSRApp
} from 'vue'
import App from './App.vue'
export function createApp() {
	const app = createSSRApp(App)
	return {
		app
	}
}
// #endif
