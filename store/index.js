import Vue from "vue"
import Vuex from "vuex"

Vue.use(Vuex)

export default new Vuex.Store({
	state: {
		userInfo: null,
		token: null
	},
	actions: {
		// 持久化数据
		init({
			state
		}, data) {
			const userInfo = uni.getStorageSync('userInfo')
			if (userInfo) {
				state.userInfo = JSON.parse(userInfo)
				state.token = JSON.parse(userInfo).token
			}
		},
		login({
			state
		}, userInfo) {
			state.userInfo = userInfo
			state.token = userInfo.token
			uni.setStorageSync('userInfo', JSON.stringify(userInfo))
			uni.$emit('userLogin', userInfo)
		},
		loginOut({
			state
		}, data) {
			state.userInfo = null
			state.token = null
			uni.removeStorageSync('userInfo')
			uni.$emit('userLoginOut', data)
		},
		updateUserInfo({
			state
		}, values) {
			Object.keys(values).forEach(k => state.userInfo[k] = values[k])
			uni.setStorageSync('userInfo', JSON.stringify(state.userInfo))
		}

	}

})
